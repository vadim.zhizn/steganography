﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace Steganography
{
    public partial class MainWindow : Form
    {
        Bitmap imageToEncrypt;
        Bitmap encryptedImage;
        Bitmap imageToAttack;
        Bitmap attackedImage;

        string fileDialogFilter = "Bmp files (*.bmp)|*.bmp|All files (*.*)|*.*";
        public MainWindow()
        {
            InitializeComponent();
            openToEncrypt.Click += (a, e) =>
            {
                imageToEncrypt = OpenFile();
                pbEncrypt.Image = imageToEncrypt;
            };
            saveEncryptedImage.Click += (a, e) => SaveFile(encryptedImage);
            openToDecrypt.Click += (a, e) => pbDecrypt.Image = OpenFile();
            openFirstImage.Click += (a, e) => firstImageToCompare.Image = OpenFile();
            openSecondImage.Click += (a, e) => secondImageToCompare.Image = OpenFile();
            openToAttack.Click += (a, e) =>
            {
                imageToAttack = OpenFile();
                pbAttack.Image = imageToAttack;
            };
            saveAttacked.Click += (a, e) => SaveFile(attackedImage);
        }
        public Bitmap OpenFile()
        {
            var fileDialog = new OpenFileDialog();
            fileDialog.Filter = fileDialogFilter;
            if (fileDialog.ShowDialog() == DialogResult.Cancel)
            {
                return null;
            }

            using var stream = fileDialog.OpenFile();
            return new Bitmap(stream);
        }
        public void SaveFile(Bitmap image)
        {
            if (image == null)
            {
                MessageBox.Show("Not found encrypted image!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var fileDialog = new SaveFileDialog();
            fileDialog.Filter = fileDialogFilter;
            if (fileDialog.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }

            using var stream = File.Open(fileDialog.FileName, FileMode.OpenOrCreate);
            image.Save(stream, ImageFormat.Bmp);
        }
        public void AddEncryptor(BaseEncryptor encryptor)
        {
            encryptionSelect.Items.Add(encryptor);
            if (encryptionSelect.SelectedIndex == -1)
                encryptionSelect.SelectedIndex = 0;
        }
        public void AddDecryptor(BaseDecryptor decryptor)
        {
            decryptionSelect.Items.Add(decryptor);
            if (decryptionSelect.SelectedIndex == -1)
                decryptionSelect.SelectedIndex = 0;
        }
        public void AddAttacker(BaseAttack attack)
        {
            attackSelect.Items.Add(attack);
            if (attackSelect.SelectedIndex == -1)
                attackSelect.SelectedIndex = 0;
        }
        public void Encrypt(object sender, EventArgs empty)
        {
            if (imageToEncrypt == null)
            {
                MessageBox.Show("Not found image to encrypt!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var image = Convertors.Bitmap2Image(imageToEncrypt);
            var encryptor = (BaseEncryptor)encryptionSelect.SelectedItem;
            var text = textToEncrypt.Text;
            encryptedImage = Convertors.Image2Bitmap(encryptor.Encrypt(image, text));
        }
        public void Decrypt(object sender, EventArgs empty)
        {
            if(pbDecrypt.Image == null)
            {
                MessageBox.Show("Not found image to decrypt!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var image = Convertors.Bitmap2Image((Bitmap)pbDecrypt.Image);
            var decryptor = (BaseDecryptor)decryptionSelect.SelectedItem;
            var text = decryptor.Decrypt(image);
            decryptedText.Text = text;
        }
        public void Attack(object sender, EventArgs empty)
        {
            if(imageToAttack == null)
            {
                MessageBox.Show("Not found image to attack!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var image = Convertors.Bitmap2Image(imageToAttack);
            var attack = (BaseAttack)attackSelect.SelectedItem;
            attackedImage = Convertors.Image2Bitmap(attack.Attack(image));
            pbAttack.Image = attackedImage;
        }
        public void Analyze(object sender, EventArgs empty)
        {
            var first = (Bitmap)firstImageToCompare.Image;
            var second = (Bitmap)secondImageToCompare.Image;
            if(first == null || second == null)
            {
                return;
            }
            var stats = ImageAnalyzer.GetMetrics(Convertors.Bitmap2Image(first), Convertors.Bitmap2Image(second));
            analyzeResult.Text = "AVE = " + stats.AveragePixelError
                                  + "\nBER = " + stats.BitErrorRate
                                  + "\nMSE = " + stats.MeanSquareError
                                  + "\nPSNR = " + stats.PickSignalToNoiseRatio
                                  + "\nRMSE = " + stats.RootMeanSquareError;
        }
    }
}
