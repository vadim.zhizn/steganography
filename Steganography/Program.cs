using System;
using System.Windows.Forms;
namespace Steganography
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var window = new MainWindow();
            window.AddEncryptor(new LsbEncryptor(new StraightInterval()));
            window.AddEncryptor(new LsbEncryptor(new PseudoRandomInterval()));
            window.AddEncryptor(new KutterJordanBossenEncryptor());

            window.AddDecryptor(new LsbDecryptor(new StraightInterval()));
            window.AddDecryptor(new LsbDecryptor(new PseudoRandomInterval()));
            window.AddDecryptor(new KutterJordanBossenDecryptor());

            window.AddAttacker(new BrightnessAttack());
            Application.Run(window);
        }
    }
}
