﻿using System;

namespace Steganography
{
    public struct Pixel
    {
        public byte R { get; set; }
        public byte G { get; set; }
        public byte B { get; set; }

        public Pixel(byte r, byte g, byte b)
        {
            R = r;
            G = g;
            B = b;
        }
        public byte this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0:
                        return R;
                    case 1:
                        return G;
                    case 2:
                        return B;
                    default:
                        throw new IndexOutOfRangeException();
                }
            }
            set
            {
                switch (index)
                {
                    case 0:
                        R = value;
                        break;
                    case 1:
                        G = value;
                        break;
                    case 2:
                        B = value;
                        break;
                    default:
                        throw new IndexOutOfRangeException();
                }
            }
        }
        public static byte Trim(int channelValue)
        {
            if (channelValue < 0)
                return 0;
            if (channelValue > 255)
                return 255;
            return (byte)channelValue;
        }
        public static byte Trim(double channelValue)
        {
            int x = (int)Math.Round(channelValue);
            if (x < 0)
                return 0;
            if (x > 255)
                return 255;
            return (byte)x;
        }
        public static byte ChangeLastBit(byte channelValue, bool bit)
        {
            channelValue &= 254;
            if (bit)
                channelValue++;
            return channelValue;
        }
    }
}
