﻿using System.Drawing;

namespace Steganography
{
    public class Image
    {
        public readonly int Width;
        public readonly int Height;
        public readonly Pixel[,] Data;
        public readonly Size Size;

        public Image(int width, int height)
        {
            Width = width;
            Height = height;
            Data = new Pixel[width, height];
            Size = new Size(width, height);
        }
        public Image(Size size)
        {
            Width = size.Width;
            Height = size.Height;
            Size = size;
            Data = new Pixel[Width, Height];
        }
        public Image(Image image)
        {
            Width = image.Width;
            Height = image.Height;
            Size = image.Size;
            Data = new Pixel[Width, Height];
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    Data[x, y] = image.Data[x, y];
                }
            }
        }
        public Pixel this[int x, int y]
        {
            get
            {
                return Data[x, y];
            }
            set
            {
                Data[x, y] = value;
            }
        }
    }
}
