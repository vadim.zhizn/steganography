﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Steganography
{
    public static class Convertors
    {
        public static Image Bitmap2Image(Bitmap bmp)
        {
            var image = new Image(bmp.Width, bmp.Height);
            for(int x = 0; x < image.Width; x++)
            {
                for(int y = 0; y < image.Height; y++)
                {
                    var pixel = bmp.GetPixel(x, y);
                    image[x, y] = new Pixel(pixel.R, pixel.G, pixel.B);
                }
            }
            return image;
        }
        public static Bitmap Image2Bitmap(Image image)
        {
            var bmp = new Bitmap(image.Width, image.Height);
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    var pixel = image[x, y];
                    bmp.SetPixel(x, y, Color.FromArgb(pixel.R, pixel.G, pixel.B));
                }
            }
            return bmp;
        }
    }
}
