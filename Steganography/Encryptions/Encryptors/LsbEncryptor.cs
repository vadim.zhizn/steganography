﻿using System;
using System.Collections;
using System.Text;

namespace Steganography
{
    public class LsbEncryptor: BaseEncryptor
    {
        private IInterval interval;
        private string name;
        public LsbEncryptor(IInterval intervalType)
        {
            interval = intervalType;
            name = intervalType.ToString();
        }
        public override Image Encrypt(Image image, string text)
        {
            if (image == null)
                throw new NullReferenceException();
            var result = new Image(image);
            text = PrepareText(text);
            var bits = new BitArray(UnicodeEncoding.Unicode.GetBytes(text));
            var count = 0;
            int x = 0, y = 0;
            while(count < bits.Length)
            {
                var pixel = image[x, y];
                pixel.R = Pixel.ChangeLastBit(pixel.R, bits[count++]);
                pixel.G = Pixel.ChangeLastBit(pixel.G, bits[count++]);
                pixel.B = Pixel.ChangeLastBit(pixel.B, bits[count++]);
                result[x, y] = pixel;
                (x, y) = interval.GetNextCoordinates(x, y, result.Width, result.Height);
                if (x == -1)
                    break;
            }
            (interval as PseudoRandomInterval)?.Reset();
            return result;
        }
        public override string ToString() => name;
    }
}
