﻿using System;
using System.Collections;
using System.Text;

namespace Steganography
{
    public class KutterJordanBossenEncryptor : BaseEncryptor
    {
        public readonly double Lambda;
        public readonly int EmbeddingCount;

        private Encoding encoding = Encoding.Unicode;
        private const int sigma = 3;

        public KutterJordanBossenEncryptor()
        {
            Lambda = 0.1;
            EmbeddingCount = 15;
        }
        public KutterJordanBossenEncryptor(double lambda, int embeddingCount)
        {
            Lambda = lambda;
            EmbeddingCount = embeddingCount;
        }
        public override Image Encrypt(Image image, string text)
        {
            if (image == null)
                throw new NullReferenceException();

            text = PrepareText(text);
            
            var result = new Image(image);
            var random = new Random(228);
            var bits = new BitArray(encoding.GetBytes(text));
            var count = 0;

            while (count < bits.Length)
            {
                var x = random.Next(sigma, result.Width - sigma);
                var y = random.Next(sigma, result.Height - sigma);
                var pixel = image[x, y];
                for (int i = 0; i < EmbeddingCount; i++)
                {
                    var Y = 0.3 * pixel.R + 0.59 * pixel.G + 0.11 * pixel.B;
                    if (Y.Equals(0))
                        Y = 50 / Lambda;
                    if (bits[count])
                    {
                        pixel.B = Pixel.Trim(pixel.B + Y * Lambda);
                    }
                    else
                    {
                        pixel.B = Pixel.Trim(pixel.B - Y * Lambda);
                    }
                }
                count++;
                result[x, y] = pixel;
            }
            return result;
        }
        public override string ToString() => "Kutter Jordan Bossen";
    }
}
