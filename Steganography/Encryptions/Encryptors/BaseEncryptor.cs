﻿namespace Steganography
{
    public abstract class BaseEncryptor
    {
        public abstract Image Encrypt(Image image, string text);

        protected string PrepareText(string text)
        {
            text = "/" + text.Length + "/" + text;
            var remainder = text.Length % 3;
            if (remainder > 0)
                text += new string(' ', 3 - remainder);
            return text;
        }
    }
}
