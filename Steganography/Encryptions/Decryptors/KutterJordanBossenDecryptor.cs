﻿using System;
using System.Collections;

namespace Steganography
{
    class KutterJordanBossenDecryptor : BaseDecryptor
    {
        Random random;
        int sigma = 3;

        public override string Decrypt(Image image)
        {
            if (image == null)
                throw new NullReferenceException();
            random = new Random(228);
            var length = GetLength(image);

            var unicodeSymbol = new BitArray(16);
            var count = 0;
            var message = "";
            while (length > 0)
            {
                var x = random.Next(sigma, image.Width - sigma);
                var y = random.Next(sigma, image.Height - sigma);
                var pixel = image[x, y];

                var b = -2.0 * pixel.B;
                for (int i = -sigma; i <= sigma; i++)
                {
                    b += image[x + i, y].B;
                    b += image[x, y + i].B;
                }
                b = b / (4 * sigma);
                bool bit = pixel.B >= Pixel.Trim(b) ? true : false;
                unicodeSymbol[count++] = bit;

                if (count == 16)
                {
                    message += DecodeSymbol(unicodeSymbol);
                    count = 0;
                    length--;
                }
            }
            
            return String.IsNullOrEmpty(message)?"Not found": message;
        }
        private int GetLength(Image image)
        {
            var unicodeSymbol = new BitArray(16);
            var length = "";
            var count = 0;
            var firstSlashFounded = false;
            while (true)
            {
                var x = random.Next(sigma, image.Width - sigma);
                var y = random.Next(sigma, image.Height - sigma);
                var pixel = image[x, y];

                var b = -2.0 * pixel.B;
                for (int i = -sigma; i <= sigma; i++)
                {
                    b += image[x + i, y].B;
                    b += image[x, y + i].B;
                }
                b = b / (4 * sigma);
                bool bit = pixel.B >= Pixel.Trim(b) ? true : false;
                unicodeSymbol[count++] = bit;
                if (count == 16)
                {
                    var symbol = DecodeSymbol(unicodeSymbol);
                    if (symbol == "/")
                    {
                        if (firstSlashFounded)
                            break;
                        firstSlashFounded = true;
                    }
                    else
                    {
                        length += symbol;
                    }
                    count = 0;
                    if (!firstSlashFounded)
                    {
                        length = "-1";
                        break;
                    }
                }
            }
            int result;
            if (!int.TryParse(length, out result))
                return -1;
            return result;
        }
        public override string ToString() => "Kutter Jordan Bossen";
    }
}
