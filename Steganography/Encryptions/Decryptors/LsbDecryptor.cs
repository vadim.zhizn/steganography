﻿using System;
using System.Collections;

namespace Steganography
{
    public class LsbDecryptor : BaseDecryptor
    {
        private IInterval interval;
        private string name;
        class Parameters
        {
            public int X { get; set; }
            public int Y { get; set; }
            public int Channel { get; set; }
            public int Lenght { get; set; }
        }
        public LsbDecryptor(IInterval intervalType)
        {
            interval = intervalType;
            name = intervalType.ToString();
        }
        public override string Decrypt(Image image)
        {
            if (image == null)
                throw new NullReferenceException();
            var parameters = GetParametersForDecoding(image);
            var x = parameters.X;
            var y = parameters.Y;
            var channel = parameters.Channel;
            var length = parameters.Lenght;
            var unicodeSymbol = new BitArray(16);
            var count = 0;
            var message = "";
            while(length > 0)
            {
                var pixel = image[x, y];
                for(; channel < 3; channel++)
                {
                    if (count == 16)
                    {
                        message += DecodeSymbol(unicodeSymbol);
                        count = 0;
                        length--;
                        break;
                    }
                    unicodeSymbol[count++] = pixel[channel] % 2 == 1 ? true : false;
                }
                if (channel == 3)
                {
                    channel = 0;
                    (x, y) = interval.GetNextCoordinates(x, y, image.Width, image.Height);
                    if (x == -1)
                        break;
                }
            }
            (interval as PseudoRandomInterval)?.Reset();
            return string.IsNullOrEmpty(message) ? "Not found" : message;
        }
        private Parameters GetParametersForDecoding(Image image)
        {
            var unicodeSymbol = new BitArray(16);
            var length = "";
            
            var count = 0;
            int x = 0, y = 0, channel = 0;
            var firstSlashFounded = false;
            while(true)
            {
                var pixel = image[x, y];
                for (; channel < 3; channel++)
                {
                    if (count == 16)
                        break;
                    unicodeSymbol[count++] = pixel[channel] % 2 == 1 ? true : false;
                }
                if (channel == 3)
                {
                    channel = 0;
                    (x, y) = interval.GetNextCoordinates(x, y, image.Width, image.Height);
                    if (x == -1)
                        break;
                }
                if(count == 16)
                {
                    var symbol = DecodeSymbol(unicodeSymbol);
                    if (symbol == "/")
                    {
                        if (firstSlashFounded)
                            break;
                        firstSlashFounded = true;
                    }
                    else
                        length += symbol;
                    count = 0;
                    if (!firstSlashFounded)
                    {
                        length = "-1";
                        break;
                    }
                }

            }
            int result;
            if (!int.TryParse(length, out result))
                result = -1;
            return new Parameters { X = x, Y = y, Channel = channel, Lenght = result };
        }
        public override string ToString() => name;
    }
}
