﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Steganography
{
    public abstract class BaseDecryptor
    {
        public abstract string Decrypt(Image image);
        protected string DecodeSymbol(BitArray bits)
        {
            byte[] tmp = new byte[2];
            bits.CopyTo(tmp, 0);
            return Encoding.Unicode.GetString(tmp);
        }
    }
}
