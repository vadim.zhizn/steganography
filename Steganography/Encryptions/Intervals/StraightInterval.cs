﻿using System;

namespace Steganography
{
    public class StraightInterval : IInterval
    {
        public Tuple<int, int> GetNextCoordinates(int x, int y, int xLimit, int yLimit)
        {
            if(++y == yLimit)
            {
                y = 0;
                x++;
            }
            if(x == xLimit)
            {
                x = -1;
                y = -1;
            }
            return Tuple.Create(x, y);
        }
        public override string ToString()
        {
            return "LSB";
        }
    }
}
