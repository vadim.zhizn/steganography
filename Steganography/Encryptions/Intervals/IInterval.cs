﻿using System;

namespace Steganography
{
    public interface IInterval
    {
        public Tuple<int, int> GetNextCoordinates(int x, int y, int xLimit, int yLimit);
    }
}
