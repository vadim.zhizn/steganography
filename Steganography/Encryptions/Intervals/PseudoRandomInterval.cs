﻿using System;

namespace Steganography
{
    public class PseudoRandomInterval : IInterval
    {
        private int seed = 228;
        private Random random;

        public PseudoRandomInterval()
        {
            random = new Random(seed);
        }
        public void Reset() => random = new Random(seed);
        public Tuple<int, int> GetNextCoordinates(int x, int y, int xLimit, int yLimit)
        {
            x = random.Next(0, xLimit);
            y = random.Next(0, yLimit);
            return Tuple.Create(x, y);
        }
        public override string ToString()
        {
            return "PRI";
        }
    }
}
