﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Steganography
{
    public class ImageMetrics
    {
        public double MeanSquareError { get; set; }
        public double RootMeanSquareError { get; set; }
        public double PickSignalToNoiseRatio { get; set; }
        public double BitErrorRate { get; set; }
        public double AveragePixelError { get; set; }
    }
}
