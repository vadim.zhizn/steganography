﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Steganography
{
    public static class ImageAnalyzer
    {
        public static ImageMetrics GetMetrics(Image firstImage, Image secondImage)
        {
            if (firstImage == null || secondImage == null)
                throw new NullReferenceException();
            if (firstImage.Height != secondImage.Height || firstImage.Width != secondImage.Width)
                throw new ArgumentException();

            var totalPixels = firstImage.Height * firstImage.Width;
            var squaredDifference = 0.0;
            var differenceSum = 0.0;
            var differentBitsCount = 0;
            for (int x = 0; x < firstImage.Width; x++)
            {
                for (int y = 0; y < firstImage.Height; y++)
                {
                    for (int channel = 0; channel < 3; channel++)
                    {
                        var tmp = Math.Abs(firstImage[x, y][channel] - secondImage[x, y][channel]);
                        if (tmp != 0)
                            differentBitsCount++;
                        differenceSum += tmp;
                        squaredDifference += tmp * tmp;
                    }
                }
            }
            return new ImageMetrics
            {
                AveragePixelError = differenceSum / totalPixels,
                PickSignalToNoiseRatio = 10 * Math.Log10(totalPixels / squaredDifference * 255 * 255 * 255),
                MeanSquareError = squaredDifference / totalPixels,
                RootMeanSquareError = Math.Sqrt(squaredDifference / totalPixels),
                BitErrorRate = differentBitsCount / (double)totalPixels * 100,
            };
        }
    }
}
