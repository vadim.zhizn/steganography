﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Steganography
{
    public abstract class BaseAttack
    {
        public abstract Image Attack(Image image);
    }
}
