﻿namespace Steganography
{
    class BrightnessAttack : BaseAttack
    {
        public override Image Attack(Image image)
        {
            var result = new Image(image.Width, image.Height);
            for(int x = 0; x < result.Width; x++)
            {
                for(int y = 0; y < result.Height; y++)
                {
                    result[x, y] = ChangeColor(image[x, y]);
                }
            }
            return result;
        }
        private Pixel ChangeColor(Pixel pixel)
        {
            var result = new Pixel();

            result.R = pixel.R % 2 == 1 ? (byte)255 : (byte)0;
            result.G = pixel.G % 2 == 1 ? (byte)255 : (byte)0;
            result.B = pixel.B % 2 == 1 ? (byte)255 : (byte)0;
            return result;
        }
        public override string ToString() => "Brightness attack";
    }
}
