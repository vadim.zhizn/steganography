﻿
namespace Steganography
{
    partial class MainWindow
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.saveEncryptedImage = new System.Windows.Forms.Button();
            this.openToEncrypt = new System.Windows.Forms.Button();
            this.encryptButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.encryptionSelect = new System.Windows.Forms.ComboBox();
            this.textToEncrypt = new System.Windows.Forms.RichTextBox();
            this.pbEncrypt = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.openToDecrypt = new System.Windows.Forms.Button();
            this.decryptButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.decryptionSelect = new System.Windows.Forms.ComboBox();
            this.decryptedText = new System.Windows.Forms.RichTextBox();
            this.pbDecrypt = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.saveAttacked = new System.Windows.Forms.Button();
            this.openToAttack = new System.Windows.Forms.Button();
            this.attackButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.attackSelect = new System.Windows.Forms.ComboBox();
            this.pbAttack = new System.Windows.Forms.PictureBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.openSecondImage = new System.Windows.Forms.Button();
            this.openFirstImage = new System.Windows.Forms.Button();
            this.analyzeButton = new System.Windows.Forms.Button();
            this.analyzeResult = new System.Windows.Forms.RichTextBox();
            this.secondImageToCompare = new System.Windows.Forms.PictureBox();
            this.firstImageToCompare = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbEncrypt)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDecrypt)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAttack)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.secondImageToCompare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstImageToCompare)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 450);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.saveEncryptedImage);
            this.tabPage1.Controls.Add(this.openToEncrypt);
            this.tabPage1.Controls.Add(this.encryptButton);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.encryptionSelect);
            this.tabPage1.Controls.Add(this.textToEncrypt);
            this.tabPage1.Controls.Add(this.pbEncrypt);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(792, 422);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Encryption";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // saveEncryptedImage
            // 
            this.saveEncryptedImage.Location = new System.Drawing.Point(8, 112);
            this.saveEncryptedImage.Name = "saveEncryptedImage";
            this.saveEncryptedImage.Size = new System.Drawing.Size(146, 23);
            this.saveEncryptedImage.TabIndex = 6;
            this.saveEncryptedImage.Text = "Save image";
            this.saveEncryptedImage.UseVisualStyleBackColor = true;
            // 
            // openToEncrypt
            // 
            this.openToEncrypt.Location = new System.Drawing.Point(8, 53);
            this.openToEncrypt.Name = "openToEncrypt";
            this.openToEncrypt.Size = new System.Drawing.Size(146, 23);
            this.openToEncrypt.TabIndex = 5;
            this.openToEncrypt.Text = "Open image";
            this.openToEncrypt.UseVisualStyleBackColor = true;
            // 
            // encryptButton
            // 
            this.encryptButton.Location = new System.Drawing.Point(8, 82);
            this.encryptButton.Name = "encryptButton";
            this.encryptButton.Size = new System.Drawing.Size(147, 24);
            this.encryptButton.TabIndex = 4;
            this.encryptButton.Text = "Encrypt";
            this.encryptButton.UseVisualStyleBackColor = true;
            this.encryptButton.Click += new System.EventHandler(this.Encrypt);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Encryption method";
            // 
            // encryptionSelect
            // 
            this.encryptionSelect.FormattingEnabled = true;
            this.encryptionSelect.Location = new System.Drawing.Point(8, 24);
            this.encryptionSelect.Name = "encryptionSelect";
            this.encryptionSelect.Size = new System.Drawing.Size(147, 23);
            this.encryptionSelect.TabIndex = 2;
            // 
            // textToEncrypt
            // 
            this.textToEncrypt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textToEncrypt.Location = new System.Drawing.Point(161, 306);
            this.textToEncrypt.Name = "textToEncrypt";
            this.textToEncrypt.Size = new System.Drawing.Size(623, 110);
            this.textToEncrypt.TabIndex = 1;
            this.textToEncrypt.Text = "";
            // 
            // pbEncrypt
            // 
            this.pbEncrypt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbEncrypt.BackColor = System.Drawing.Color.SkyBlue;
            this.pbEncrypt.Location = new System.Drawing.Point(161, 6);
            this.pbEncrypt.Name = "pbEncrypt";
            this.pbEncrypt.Size = new System.Drawing.Size(623, 294);
            this.pbEncrypt.TabIndex = 0;
            this.pbEncrypt.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.openToDecrypt);
            this.tabPage2.Controls.Add(this.decryptButton);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.decryptionSelect);
            this.tabPage2.Controls.Add(this.decryptedText);
            this.tabPage2.Controls.Add(this.pbDecrypt);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(792, 422);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Decryption";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // openToDecrypt
            // 
            this.openToDecrypt.Location = new System.Drawing.Point(8, 53);
            this.openToDecrypt.Name = "openToDecrypt";
            this.openToDecrypt.Size = new System.Drawing.Size(146, 23);
            this.openToDecrypt.TabIndex = 10;
            this.openToDecrypt.Text = "Open image";
            this.openToDecrypt.UseVisualStyleBackColor = true;
            // 
            // decryptButton
            // 
            this.decryptButton.Location = new System.Drawing.Point(8, 82);
            this.decryptButton.Name = "decryptButton";
            this.decryptButton.Size = new System.Drawing.Size(147, 24);
            this.decryptButton.TabIndex = 9;
            this.decryptButton.Text = "Decrypt";
            this.decryptButton.UseVisualStyleBackColor = true;
            this.decryptButton.Click += new System.EventHandler(this.Decrypt);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "Decryption method";
            // 
            // decryptionSelect
            // 
            this.decryptionSelect.FormattingEnabled = true;
            this.decryptionSelect.Location = new System.Drawing.Point(8, 24);
            this.decryptionSelect.Name = "decryptionSelect";
            this.decryptionSelect.Size = new System.Drawing.Size(147, 23);
            this.decryptionSelect.TabIndex = 7;
            // 
            // decryptedText
            // 
            this.decryptedText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.decryptedText.Location = new System.Drawing.Point(161, 306);
            this.decryptedText.Name = "decryptedText";
            this.decryptedText.ReadOnly = true;
            this.decryptedText.Size = new System.Drawing.Size(623, 110);
            this.decryptedText.TabIndex = 2;
            this.decryptedText.Text = "";
            // 
            // pbDecrypt
            // 
            this.pbDecrypt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbDecrypt.BackColor = System.Drawing.Color.SkyBlue;
            this.pbDecrypt.Location = new System.Drawing.Point(161, 6);
            this.pbDecrypt.Name = "pbDecrypt";
            this.pbDecrypt.Size = new System.Drawing.Size(623, 294);
            this.pbDecrypt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbDecrypt.TabIndex = 1;
            this.pbDecrypt.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.saveAttacked);
            this.tabPage3.Controls.Add(this.openToAttack);
            this.tabPage3.Controls.Add(this.attackButton);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.attackSelect);
            this.tabPage3.Controls.Add(this.pbAttack);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(792, 422);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Attack";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // saveAttacked
            // 
            this.saveAttacked.Location = new System.Drawing.Point(8, 112);
            this.saveAttacked.Name = "saveAttacked";
            this.saveAttacked.Size = new System.Drawing.Size(146, 23);
            this.saveAttacked.TabIndex = 17;
            this.saveAttacked.Text = "Save image";
            this.saveAttacked.UseVisualStyleBackColor = true;
            // 
            // openToAttack
            // 
            this.openToAttack.Location = new System.Drawing.Point(8, 53);
            this.openToAttack.Name = "openToAttack";
            this.openToAttack.Size = new System.Drawing.Size(146, 23);
            this.openToAttack.TabIndex = 16;
            this.openToAttack.Text = "Open image";
            this.openToAttack.UseVisualStyleBackColor = true;
            // 
            // attackButton
            // 
            this.attackButton.Location = new System.Drawing.Point(8, 82);
            this.attackButton.Name = "attackButton";
            this.attackButton.Size = new System.Drawing.Size(147, 24);
            this.attackButton.TabIndex = 15;
            this.attackButton.Text = "Attack";
            this.attackButton.UseVisualStyleBackColor = true;
            this.attackButton.Click += new System.EventHandler(this.Attack);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 15);
            this.label3.TabIndex = 14;
            this.label3.Text = "Attack method";
            // 
            // attackSelect
            // 
            this.attackSelect.FormattingEnabled = true;
            this.attackSelect.Location = new System.Drawing.Point(8, 24);
            this.attackSelect.Name = "attackSelect";
            this.attackSelect.Size = new System.Drawing.Size(147, 23);
            this.attackSelect.TabIndex = 13;
            // 
            // pbAttack
            // 
            this.pbAttack.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbAttack.BackColor = System.Drawing.Color.SkyBlue;
            this.pbAttack.Location = new System.Drawing.Point(161, 6);
            this.pbAttack.Name = "pbAttack";
            this.pbAttack.Size = new System.Drawing.Size(623, 408);
            this.pbAttack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbAttack.TabIndex = 11;
            this.pbAttack.TabStop = false;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.openSecondImage);
            this.tabPage4.Controls.Add(this.openFirstImage);
            this.tabPage4.Controls.Add(this.analyzeButton);
            this.tabPage4.Controls.Add(this.analyzeResult);
            this.tabPage4.Controls.Add(this.secondImageToCompare);
            this.tabPage4.Controls.Add(this.firstImageToCompare);
            this.tabPage4.Location = new System.Drawing.Point(4, 24);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(792, 422);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Analyzing";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // openSecondImage
            // 
            this.openSecondImage.Location = new System.Drawing.Point(8, 312);
            this.openSecondImage.Name = "openSecondImage";
            this.openSecondImage.Size = new System.Drawing.Size(146, 23);
            this.openSecondImage.TabIndex = 23;
            this.openSecondImage.Text = "Open second";
            this.openSecondImage.UseVisualStyleBackColor = true;
            // 
            // openFirstImage
            // 
            this.openFirstImage.Location = new System.Drawing.Point(8, 283);
            this.openFirstImage.Name = "openFirstImage";
            this.openFirstImage.Size = new System.Drawing.Size(146, 23);
            this.openFirstImage.TabIndex = 22;
            this.openFirstImage.Text = "Open first";
            this.openFirstImage.UseVisualStyleBackColor = true;
            // 
            // analyzeButton
            // 
            this.analyzeButton.Location = new System.Drawing.Point(8, 341);
            this.analyzeButton.Name = "analyzeButton";
            this.analyzeButton.Size = new System.Drawing.Size(146, 23);
            this.analyzeButton.TabIndex = 21;
            this.analyzeButton.Text = "Analyze";
            this.analyzeButton.UseVisualStyleBackColor = true;
            this.analyzeButton.Click += new System.EventHandler(this.Analyze);
            // 
            // analyzeResult
            // 
            this.analyzeResult.Location = new System.Drawing.Point(160, 283);
            this.analyzeResult.Name = "analyzeResult";
            this.analyzeResult.Size = new System.Drawing.Size(626, 133);
            this.analyzeResult.TabIndex = 20;
            this.analyzeResult.Text = "";
            // 
            // secondImageToCompare
            // 
            this.secondImageToCompare.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.secondImageToCompare.BackColor = System.Drawing.Color.SkyBlue;
            this.secondImageToCompare.Location = new System.Drawing.Point(400, 7);
            this.secondImageToCompare.Name = "secondImageToCompare";
            this.secondImageToCompare.Size = new System.Drawing.Size(386, 270);
            this.secondImageToCompare.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.secondImageToCompare.TabIndex = 19;
            this.secondImageToCompare.TabStop = false;
            // 
            // firstImageToCompare
            // 
            this.firstImageToCompare.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.firstImageToCompare.BackColor = System.Drawing.Color.SkyBlue;
            this.firstImageToCompare.Location = new System.Drawing.Point(8, 7);
            this.firstImageToCompare.Name = "firstImageToCompare";
            this.firstImageToCompare.Size = new System.Drawing.Size(386, 270);
            this.firstImageToCompare.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.firstImageToCompare.TabIndex = 18;
            this.firstImageToCompare.TabStop = false;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.Name = "MainWindow";
            this.Text = "Steganography";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbEncrypt)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDecrypt)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAttack)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.secondImageToCompare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstImageToCompare)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button encryptButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox encryptionSelect;
        private System.Windows.Forms.RichTextBox textToEncrypt;
        private System.Windows.Forms.PictureBox pbEncrypt;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button saveEncryptedImage;
        private System.Windows.Forms.Button openToEncrypt;
        private System.Windows.Forms.Button openToDecrypt;
        private System.Windows.Forms.Button decryptButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox decryptionSelect;
        private System.Windows.Forms.RichTextBox decryptedText;
        private System.Windows.Forms.PictureBox pbDecrypt;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button saveAttacked;
        private System.Windows.Forms.Button openToAttack;
        private System.Windows.Forms.Button attackButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox attackSelect;
        private System.Windows.Forms.PictureBox pbAttack;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button analyzeButton;
        private System.Windows.Forms.RichTextBox analyzeResult;
        private System.Windows.Forms.PictureBox secondImageToCompare;
        private System.Windows.Forms.PictureBox firstImageToCompare;
        private System.Windows.Forms.Button openSecondImage;
        private System.Windows.Forms.Button openFirstImage;
    }
}

